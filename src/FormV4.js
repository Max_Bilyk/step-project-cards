class Form {
    constructor(inputsProps, inputsId, inputsTypes, textAreasProps, textAreasId) {
        this.inputsProps = inputsProps;
        this.inputsId = inputsId;
        this.inputsTypes = inputsTypes;
        this.textAreasProps = textAreasProps;
        this.textAreasId = textAreasId;
    }

    static chooseDoctor(container) {
        container.insertAdjacentHTML('beforeend', `<form id="visitForm"></form>`);

        const visitForm = document.getElementById('visitForm');
        const doctorSelection = new Select('chosenDoctor', 'Doctor', ['Therapist', 'Cardiologist', 'Dentist']).createSelect();
        this.renderSelect(doctorSelection, visitForm);
        const doctor = document.getElementById('chosenDoctor');

        doctor.addEventListener('change', (e) => {

            const modalFooter = document.querySelector('#modalFooter')
            const Therapist = new TherapistForm(['Full name', 'Age'], ['patientFullName', 'patientAge'], ['text', 'number'], ['Aim of your visit', 'Short description of your visit'], ['visitAim', 'visitDescription']);
            const Cardiologist = new CardiologistForm(['Full name', 'Normal pressure', 'Body mass index', 'Age'], ['patientFullName', 'patientPressure', 'patientBodyIndex', 'patientAge'], ['text', 'number', 'number', 'number'], ['Aim of your visit', 'Short description of your visit', 'Past diseases of the cardiovascular system'], ['visitAim', 'visitDescription', 'cardiovascularDiseases']);
            const Dentist = new DentistForm(['Full name', 'Date of the last visit'], ['patientFullName', 'lastVisit'], ['text', 'date'], ['Aim of your visit', 'Short description of your visit',], ['visitAim', 'visitDescription',]);
            const children = [...visitForm.children].splice(1, [...visitForm.children].length - 1);

            switch (e.target.value) {
                case "Therapist" :
                    if (visitForm.children.length > 1) {
                        children.forEach(child => child.remove());
                    }
                    modalFooter.innerText = ''
                    Therapist.renderDoctor();
                    Therapist.renderButton('Submit', 'submit', 'submit-btn', visitForm);

                    break;
                case "Cardiologist" :
                    if (visitForm.children.length > 1) {
                        children.forEach(child => child.remove());
                    }
                    modalFooter.innerText = ''
                    Cardiologist.renderDoctor();
                    Cardiologist.renderButton('Submit', 'submit', 'submit-btn', visitForm);

                    break;
                case "Dentist" :
                    if (visitForm.children.length > 1) {
                        children.forEach(child => child.remove());
                    }
                    modalFooter.innerText = ''
                    Dentist.renderDoctor();
                    Therapist.renderButton('Submit', 'submit', 'submit-btn', visitForm);

                    break;
            }
        });
    }

    renderInput(form) {
        this.inputsProps.forEach((input, index) => {
            input = new Input().createInput();
            input.placeholder = this.inputsProps[index];
            input.id = this.inputsId[index];
            input.type = this.inputsTypes[index];
            let label = new Label()
                .createLabel(`Enter your ${input.placeholder.toLowerCase()}:`, input.id);
            let container = document.createElement('div');
            container.classList.add('form-group');
            container.append(label, input);
            form.appendChild(container);
        });
    }

    renderTextArea(form) {
        this.textAreasProps.forEach((textArea, index) => {
            textArea = new TextArea().createTextArea();
            textArea.placeholder = this.textAreasProps[index];
            textArea.id = this.textAreasId[index];
            let label = new Label()
                .createLabel(`Enter the ${textArea.placeholder.toLowerCase()}:`, textArea.id);
            let container = document.createElement('div');
            container.classList.add('form-group');
            container.append(label, textArea);
            form.appendChild(container);
        })
    }

    static renderSelect(select, form) {
        let label = new Label()
            .createLabel(`Choose the ${select.value.toLowerCase()}:`, select.id);
        let container = document.createElement('div');
        container.classList.add('form-group');
        container.append(label, select);
        form.appendChild(container);
    }

    renderButton(text, type, className, appendTo) {
        const button = document.createElement('button');

        button.innerText = text;
        button.type = type;
        button.classList.add('btn', 'btn-primary', className)
        appendTo.appendChild(button);

        const visitForm = document.querySelector('.submit-btn');
        const inputData = document.querySelectorAll('.field-data')

        visitForm.addEventListener('click', async (e) => {
                e.preventDefault();

                const serverData = this.getAllFieldsData(inputData);
                const modalFooter = document.querySelector('#modalFooter');

                sendRequest('POST', 'https://ajax.test-danit.com/api/cards', serverData)
                    .then((response) => {
                        if (response.status >= 200 && response.status < 300) {
                            modalFooter.innerHTML = `
                                <div class="alert alert-success" role="alert">
                                    Card has been saved :)
                                </div>
                                `
                        }
                    })
                    .catch((error) => {
                        modalFooter.innerHTML = `
                        <div class="alert alert-danger" role="alert">
                                ${error}. Something gone wrong :(
                        </div>`
                    })
                button.disabled = true;

                setTimeout(() => {
                    if (document.body.hasAttribute('class')) {
                        document.body.style.paddingRight = '0';
                        document.body.removeAttribute('class');
                    }
                    const backgroundBlocked = document.querySelector('.modal-backdrop');
                    backgroundBlocked.removeAttribute('class');
                    backgroundBlocked.remove();
                    const modal = document.getElementById('modalVisit');
                    modal.remove();
                }, 1500)

                sendRequest('GET', 'https://ajax.test-danit.com/api/cards')
                    .then((response) => {
                        const container = document.querySelector('.card-container');
                        const visitCard = new VisitDoctor(response.data);
                        const cardId = response.data.forEach((card) => {
                            return card.id
                        })
                        visitCard.renderCards(container, cardId);
                    });
            }
        )
        ;

        return button;
    }


    getAllFieldsData(elements) {
        const fieldsDataObj = {}
        elements.forEach((element) => {
            fieldsDataObj[element.id] = element.value;
        });
        return fieldsDataObj;
    }
}

class TherapistForm extends Form {
    constructor(inputsProps, inputsId, inputsTypes, textAreasProps, textAreasId) {
        super(inputsProps, inputsId, inputsTypes, textAreasProps, textAreasId);
    }

    renderDoctor() {
        const therapistName = new Select('therapistName', 'Therapist name', [
            'Fedor Stepanov',
            'Den Yaroshenko',
            'Max Bilyk'
        ]).createSelect();
        const priorityOptions = new Select('priority', 'Priority', [
            'Low',
            'Normal',
            'High'
        ]).createSelect();
        const form = document.getElementById('visitForm');
        this.renderInput(form);
        this.renderTextArea(form);
        Form.renderSelect(therapistName, form);
        Form.renderSelect(priorityOptions, form);
    }
}

class CardiologistForm extends Form {
    constructor(inputsProps, inputsId, inputsTypes, textAreasProps, textAreasId) {
        super(inputsProps, inputsId, inputsTypes, textAreasProps, textAreasId);
    }

    renderDoctor() {
        const cardiologistName = new Select('cardiologistName', 'Cardiologist name', [
            'Anton Sypavka',
            'Gleb Bondarenko',
            'Mattiev Dementiev'
        ]).createSelect();
        const priorityOptions = new Select('priority', 'Priority', [
            'Low',
            'Normal',
            'High'
        ]).createSelect();
        const form = document.getElementById('visitForm');
        this.renderInput(form);
        this.renderTextArea(form);
        Form.renderSelect(cardiologistName, form);
        Form.renderSelect(priorityOptions, form);
    }

}

class DentistForm extends Form {
    constructor(inputsProps, inputsId, inputsTypes, textAreasProps, textAreasId) {
        super(inputsProps, inputsId, inputsTypes, textAreasProps, textAreasId);
    }

    renderDoctor() {
        const dentistName = new Select('dentistName', 'Dentist name', [
            'Alexandr Patsalov',
            'Sergey Donchenko',
            'Artem Lotariev'
        ]).createSelect();
        const priorityOptions = new Select('priority', 'Priority', [
            'Low',
            'Normal',
            'High'
        ]).createSelect();
        const form = document.getElementById('visitForm');
        this.renderInput(form);
        this.renderTextArea(form);
        Form.renderSelect(dentistName, form);
        Form.renderSelect(priorityOptions, form);
    }
}