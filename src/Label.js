class Label {
    constructor() {}
    createLabel(name, forId = '') {
        const label = document.createElement('label');
        label.classList.add('col-form-label');
        label.innerText = name;
        label.htmlFor = forId;
        return label;
    }
}