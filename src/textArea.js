class TextArea {
    constructor() {}

    createTextArea() {
        const textArea = document.createElement('textarea');
        textArea.required = true;
        textArea.classList.add('form-control', 'mb-2', 'field-data');
        return textArea;
    }
}
