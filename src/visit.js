class VisitDoctor {
    constructor(visit) {
        this.chosenDoctor = visit.content.chosenDoctor;
        this.patientFullName = visit.content.patientFullName;
        this.patientAge = visit.content.patientAge;
        this.status = visit.content.status;
        this.priority = visit.content.priority;
        this.visitAim = visit.content.visitAim;
        this.visitDescription = visit.content.visitDescription;
        this.therapistName = visit.content.therapistName;
        this.patientPressure = visit.content.patientPressure;
        this.patientBodyIndex = visit.content.patientBodyIndex;
        this.cardiovascularDiseases = visit.content.cardiovascularDiseases;
        this.cardiologistName = visit.content.cardiologistName;
        this.lastVisit = visit.content.lastVisit;
        this.dentistName = visit.content.dentistName;
        this.id = visit.id;
    }

    static renderAllCards(cards) {
        document.querySelectorAll('.visit').forEach(el => {
            el.remove();
        });
        cards.forEach(card => {
                VisitDoctor.renderingFilteredCards(card, card.id);
            }
        )
    }

    static renderingFilteredCards(card, cardId) {
        const container = document.querySelector('.card-container');
        const visitCard = new VisitDoctor(card);
        visitCard.renderCards(container, cardId);
    }

    renderCards(container, id) {

        const noItems = document.querySelector('.card-container__title');
        noItems ? noItems.style.display = 'none' : '';

        const visitCard = document.createElement('div');
        visitCard.classList.add('visit');
        visitCard.id = this.id;

        visitCard.innerHTML = `
            <div class="card text-white bg-dark mb-3" style="max-width: 20rem;">

                <div class="card-header">
                    Visit Info:
                    <span  class="delete-edit-visit">
                        <i class="far fa-edit" data-toggle="modal" data-target="#modal-edit" id="${id}-edit"></i>
                        <i class="far fa-trash-alt delete" id="${id}delete"></i>          
                    </span>
                </div>

                <ul class="list-group list-group-flush">
                    <li class="list-group-item"><span>Patient name: </span> ${this.patientFullName}</li>
                    <li class="list-group-item"><span>Doctor: </span> ${this.chosenDoctor}</li>
                    <li class="list-group-item">
                        <button  type="button" data-toggle="collapse" data-target="#moreInfo${id}" aria-expanded="false" aria-controls="collapseExample" id="${id}ShowMore" class="btn btn-secondary moreInfo lessInfo">Show more</button>
                    </li>
                    <ul class="list-group collapse more-info" id="moreInfo${id}">
                        
                    </ul>
                </ul>

                <div class="card-footer">
                    <button class="btn btn-success finishVisitBtn optionBtn">Finish</button>
                </div>
            </div>
        `
        container.appendChild(visitCard);

        const cardId = document.getElementById(id);
        const deleteBtnId = document.getElementById(id + 'delete');
        const editBtnId = document.getElementById(id + '-edit');
        const moreInfoContainer = document.getElementById(`moreInfo${id}`);
        const showMoreBtn = document.getElementById(id + 'ShowMore');
        VisitDoctor.deleteVisit(deleteBtnId, cardId);
        VisitDoctor.editVisit(editBtnId, cardId);
        VisitDoctor.showMore(moreInfoContainer, showMoreBtn, cardId);

    }

    static editVisit(editBtn, card) {
        if (editBtn.id.includes(card.id)) {
            editBtn.addEventListener('click', async function () {
                const authorizeModal = document.querySelectorAll('#authorizeServiceWindow');
                const createVisitModal = document.querySelectorAll('#modalVisit');

                if (authorizeModal !== null) {
                    for (const el of authorizeModal) {
                        el.remove();
                    }
                }
                if (createVisitModal !== null) {
                    createVisitModal.forEach((el) => {
                        el.remove()
                    });
                }

                const editModal = new ModalWindowVersion2('editModal', 'Edit visit:');
                editModal.render(document.body);

                const modalWindow = document.querySelector('.modal');
                modalWindow.setAttribute('id', 'modal-edit');

            })
        }
    }

    static deleteVisit(deleteBtn, card) {
        deleteBtn.addEventListener('click', async function (e) {
            e.stopPropagation();
            if (deleteBtn.id.includes(card.id)) {
                const response = await sendRequest('DELETE', `https://ajax.test-danit.com/api/cards/${card.id}`);
                card.remove();
                alert('Thank you. Card has been deleted!');
            } else {
                alert('Something gone wrong');
            }
        })
    }


    static showMore(container, moreBtn, card) {

        if (moreBtn.id.includes(card.id)) {
            moreBtn.addEventListener('click', async function (e) {
                sendRequest('GET', 'https://ajax.test-danit.com/api/cards')
                    .then(response => {
                        const props = response.data.map(obj => {
                            return obj.content.chosenDoctor;
                        });
                        const classProps = response.data.map(item => {
                            return item;
                        });
                        switch (props[0]) {
                            case 'Therapist' :
                                const visitTherapist = new VisitTherapist();
                                visitTherapist.renderCards(container);
                        }
                    })
            })
        }
    }
}

class VisitTherapist extends VisitDoctor {
    constructor() {
        super();
    }

    renderCards(container) {
        super.renderCards();

        container.innerHTML = `
            <li class="list-group-item"><span class="visit-line">Therapist name: </span>${this.therapistName}</li>
            <li class="list-group-item"><span class="visit-line">Patient age: </span>${this.patientAge}</li>
            <li class="list-group-item"><span class="visit-line">Visit target: </span>${this.visitAim}</li>
            <li class="list-group-item"><span class="visit-line">Short description: </span>${this.visitDescription}</li>            
        `;


    }
}


class VisitCardiologist extends VisitDoctor {
    constructor() {
        super();
    }

    renderCards(container) {
        super.renderCards();

        container.insertAdjacentHTML('beforeend', `
            <li class="list-group-item"><span>Cardiologist name: </span>${this.cardiologistName}</li>
            <li class="list-group-item"><span>Patient age: </span>${this.patientAge}</li>
            <li class="list-group-item"><span>Blood pressure: </span>${this.patientPressure}</li>
            <li class="list-group-item"><span>Weight index: </span>${this.patientBodyIndex}</li>
            <li class="list-group-item"><span>Previous diseases: </span>${this.cardiovascularDiseases}</li>
        `)
        ;

    }
}


class VisitDentist extends VisitDoctor {
    constructor() {
        super();
    }

    renderCards(container) {
        super.renderCards();
        container.insertAdjacentHTML('beforeend', `
            <li class="list-group-item"><span class="visit-line">Dentist name: </span>${this.dentistName}</li>
            <li class="list-group-item"><span class="visit-line">Last visit: </span>${this.lastVisit.split('-').reverse().join('-')}</li>
            <li class="list-group-item"><span class="visit-line">Visit target: </span>${this.visitAim}</li>
            <li class="list-group-item"><span class="visit-line">Short description: </span>${this.visitDescription}</li>
        `);
    }
}

//     const renderContainer = document.querySelector('.modal-body');
//
//     if (card.chosenDoctor === 'Therapist') {
//         new TherapistForm().render(renderContainer)
//     } else if (card.chosenDoctor === 'Cardiologist') {
//         new CardiologistForm().render(renderContainer);
//     } else if (card.chosenDoctor === 'Dentist') {
//         new DentistForm().render(renderContainer)
//     }
// }
//
// finishVisit(finishBtn) {
//     const visitOptionsBtn = document.querySelector('.visitOptionsBtn');
//     this.showVisitOptions(visitOptionsBtn);
//
//     if (finishBtn.textContent === 'Finish') {
//         this.status = 'Finished';
//         this._visitCard.classList.add('visit-finished');
//
//         sendRequest('PUT', `http://cards.danit.com.ua/cards/${this.id}`, {}, null, this)
//             .then((response) => {
//                 visitsArr = visitsArr.map((card) => {
//                     return card.id === response.id ? response : card
//                 })
//                 finishBtn.textContent = 'Open';
//                 const visitEditBtn = this._visitCard.querySelector('.editVisitBtn');
//                 visitEditBtn.disabled = true;
//             }).catch((error) => {
//             console.log(error)
//             this.status = 'Opened';
//         })
//     } else {
//         this.status = 'Opened';
//
//         sendRequest('PUT', `http://cards.danit.com.ua/cards/${this.id}`, {}, null, this)
//             .then((response) => {
//                 this._visitCard.classList.remove('visit-finished');
//                 visitsArr = visitsArr.map((card) => {
//                     return card.id === response.id ? response : card
//                 });
//                 finishBtn.textContent = 'Finish';
//                 const visitEditBtn = this._visitCard.querySelector('.editVisitBtn');
//                 visitEditBtn.disabled = false;
//             })
//             .catch((error) => {
//                 console.log(error);
//                 this.status = 'Finished'
//             })
//     }
// }
//
// deleteVisit() {
//     console.log('ok')
// console.log(e.target);
// const deleteModal = new ModalWindowVersion2('delete-visit', 'Are you sure that you want to delete visit');
// const deleteModalContent = document.querySelector('.modal-body');
// deleteModal.render(document.body);
//
// deleteModalContent.innerHTML = `
//     <div class="btn-group" role="group">
//         <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
//         <button type="button" id="confirmDelete" class="btn btn-danger">Delete</button>
//     </div>
// `
// const confirmDelete = document.getElementById('confirmDelete');
// confirmDelete.addEventListener('click', function () {
//     sendRequest('DELETE', `https://ajax.test-danit.com/api/cards/${this.id}`, {}, this.id)
//         .then((response) => {
//             const modalFooter = document.querySelector('.modal-footer')
//             visitsArr.filter((card) => {
//                 return card.id !== this.id;
//             })
//             if (response.status >= 200 && response.status < 300) {
//                 modalFooter.innerHTML = `
//                 <div class="alert alert-success" role="alert">
//                     Card has been deleted
//                 </div>
//             `
//             }
//         }).catch((error) => {
//         const modalFooter = document.querySelector('.modal-footer');
//
//         modalFooter.innerHTML = `
//                     <div class="alert alert-danger" role="alert">
//                             ${error}. Something gone wrong
//                     </div>`
//     })
// })

// }
// }


// const
// showMoreBtn = document.querySelector('.showMoreBtn'),
// editVisitBtn = document.querySelector('.editVisitBtn'),
// finishVisitBtn = document.querySelector('.finishVisitBtn'),
// visitOptionsBtn = document.querySelector('.visitOptionsBtn'),
