const filterForm = document.getElementById('search-form');
const filterBtn = document.getElementById('cardsFilterBtn');

filterForm.addEventListener('submit', async function (e) {
    e.preventDefault();

    if (localStorage.getItem('authorizeToken') === null){
        const filterError = document.createElement('div');
        filterError.classList.add('alert', 'alert-danger');
        filterError.id = 'filterError';
        filterError.setAttribute('role', 'alert');
        filterError.innerText = 'Please log in your account';
        filterBtn.disabled = true;
        filterForm.after(filterError);

        return false;
    }

    let response = await sendRequest('GET', 'https://ajax.test-danit.com/api/cards')
    document.getElementsByClassName('card-container')[0].innerHTML = '';
    const filterSearcher = document.getElementById('filterField').value.toLowerCase();
    // const status = document.getElementById('visit-status').value;
    const visitPriority = document.getElementById('priority-status').value;

    const cardFilteredRender = response.data.filter((el) => {
        // const cardData = el.content;
        let {priority, visitDescription, visitAim, chosenDoctor, patientFullName} = el.content;

        return (visitPriority === priority || visitPriority === 'All')
            && (visitDescription.includes(filterSearcher)
                || !filterSearcher
                || visitAim.toLowerCase().includes(filterSearcher)
                || chosenDoctor.toLowerCase().includes(filterSearcher)
                || patientFullName.toLowerCase().includes(filterSearcher))

    })

    VisitDoctor.renderAllCards(cardFilteredRender);
})